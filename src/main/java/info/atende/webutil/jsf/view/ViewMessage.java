package info.atende.webutil.jsf.view;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * Classe utilitaria para mensagems em JSF
 * Criado por giovanni.
 * Data: 27/04/12
 * Hora: 21:22
 */
public class ViewMessage {
    private static FacesMessage msg;
    public static void error(String summary, String message){
        msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, message);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    public static void error(String message){
        msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro", message);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    public static void info(String summary, String message){
        msg = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, message);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    public static void info(String message){
        msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", message);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    public static void warn(String summary, String message){
        msg = new FacesMessage(FacesMessage.SEVERITY_WARN, summary, message);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    public static void warn(String message){
        msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", message);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    public static void fatal(String summary, String message){
        msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, summary, message);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    public static void fatal(String message){
        msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Fatal", message);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
