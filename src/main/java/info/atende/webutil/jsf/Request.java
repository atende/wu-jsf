package info.atende.webutil.jsf;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 * Utilitario para verificar a url que a requisição foi feita
 * User: giovanni
 * Date: 27/04/12
 * Time: 11:46
 *
 * @author Giovanni Candido da Silva <giovanni@giovannicandido.com>
 */
public class Request {
    /**
     * Retorna a url requisitada, completa
     *
     * @return
     */
    public static String getRequestURL() {
        Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();
        if (request instanceof HttpServletRequest) {

            return ((HttpServletRequest) request).getRequestURL().toString();
        } else {
            return "";
        }

    }

    /**
     * Retorna o endereco do servidor, considerando o contexto
     *
     * @return
     */
    public static String getBaseUrl() {
        Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();
        if (request instanceof HttpServletRequest) {
            HttpServletRequest requestServlet = (HttpServletRequest) request;
            String urlComplet = requestServlet.getScheme() + "://" + requestServlet.getServerName();
            int port = requestServlet.getServerPort();
            if (port != 80) {
                urlComplet += ":" + port;
            }

            urlComplet += requestServlet.getContextPath();
            if (!urlComplet.endsWith("/")) {
                urlComplet += "/";
            }
            return urlComplet;
        } else {
            return "";
        }
    }
}
