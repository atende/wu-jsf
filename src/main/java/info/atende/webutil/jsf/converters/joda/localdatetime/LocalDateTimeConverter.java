/*
 * Copyright (c) 2012. Atende Tecnologia da Informação.
 * Este software é propriedade de Atende Tecnologia da Informação e está protegido por
 * leis internacionais de direitos autorais.
 */

package info.atende.webutil.jsf.converters.joda.localdatetime;


import info.atende.webutil.jsf.converters.joda.Common;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;


/**
 * Um conversor para classe org.joda.time.LocalDateTimeConverter
 * @author Giovanni Candido da Silva < giovanni@giovannicandido.com >
 */

public class LocalDateTimeConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        String pattern = Common.getPattern(component);
        LocalDateTime localDateTime = null;
        try{
            localDateTime = DateTimeFormat.forPattern(pattern).parseLocalDateTime(value);
        }catch (Exception ex){
            throw new ConverterException(ex.getMessage(), ex);
        }
        return localDateTime;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String pattern = Common.getPattern(component);
        String dateTime;
        try{
            dateTime = DateTimeFormat.forPattern(pattern).print((LocalDateTime) value);
        }catch (Exception ex){
            throw  new ConverterException(ex);
        }
        return dateTime;
    }

}
