package info.atende.webutil.jsf;

import info.atende.webutil.jsf.view.ViewMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.faces.FacesException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;
import java.util.Iterator;

/**
 * Receber e tratar exceções do aplicativo
 * Cria uma mensagem de erro para JSF
 * User: giovanni
 * Date: 25/05/12
 * Time: 14:19
 *
 * @author Giovanni Candido da Silva <giovanni@giovannicandido.com>
 */

public class DefaultExceptionHandler extends ExceptionHandlerWrapper {
    private ExceptionHandler wrapped;
    private static final Log LOG = LogFactory.getLog(DefaultExceptionHandler.class);

    public DefaultExceptionHandler(ExceptionHandler wrapped) {
        this.wrapped = wrapped;
    }

    @Override
    public ExceptionHandler getWrapped() {
        return wrapped;
    }

    @Override
    public void handle() throws FacesException {
        FacesContext fc = FacesContext.getCurrentInstance();

        for (Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents().iterator(); i.hasNext(); ) {
            ExceptionQueuedEvent event = i.next();
            ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event.getSource();

            String redirectPage = null;
            fc = FacesContext.getCurrentInstance();
            Throwable t = context.getException();

            try {

                ViewMessage.error(t.getLocalizedMessage());
                LOG.error(t.getLocalizedMessage(), t);

            } finally {
                i.remove();
            }


            break;
        }
        getWrapped().handle();

    }
}
